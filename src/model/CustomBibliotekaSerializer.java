package model;

import java.io.IOException;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import model.parametar.Parametar;

/**
 *
 * Klasa koja modeluje serializer objekta biblioteka.
 * Sustina je da se ne moze objekat klase Biblioteka direktno upisati u
 * biblioteka.json fajl, nego je potrebno da se on serijalizuje na poseban
 * nacin.
 */
public class CustomBibliotekaSerializer extends StdSerializer<Biblioteka> {
	public CustomBibliotekaSerializer() {
		this(null);
	}
	
	public CustomBibliotekaSerializer(Class<Biblioteka> t) {
		super(t);
	}

	@Override
	public void serialize(Biblioteka b, JsonGenerator g, SerializerProvider p) throws IOException {
		g.writeStartObject();
		g.writeArrayFieldStart("metaUredjaji");
		for (Entry<String, MetaPametniUredjaj> e : b.getMetaUredjaji().entrySet()) {
			
			MetaPametniUredjaj m = e.getValue();
			System.out.println("Cuvam " + m.getNaziv());
			g.writeStartObject();
			// ------------------------------------------------------
			g.writeStringField("naziv", m.getNaziv());
			if (m.getIcon() != null) {
				g.writeStringField("iconPath", m.getIcon().getDescription());
			}
			else {
				g.writeStringField("iconPath", null);
			}
			g.writeArrayFieldStart("parametri");
				// ------------------------------------------------------
			for (Parametar param : m.getParametri()) {
				g.writeStartObject();
				g.writeStringField("tip", param.getTip());
				g.writeStringField("naziv", param.getNaziv());
				g.writeEndObject();
			}
			g.writeEndArray();
				// ------------------------------------------------------
			g.writeArrayFieldStart("konfiguracije");
				// ------------------------------------------------------
			if (m.getKonfiguracije() != null) {
				// Morao sam if nzm zasto je pucao bez
				for (Konfiguracija konfig : m.getKonfiguracije()) {
					g.writeStartObject();
					g.writeStringField("naziv", konfig.getNaziv());
					g.writeArrayFieldStart("parametri");
					for (Parametar param : konfig.getParametri()) {
						g.writeStartObject();
						g.writeObjectField("podatak", param.getPodatak());
						g.writeEndObject();
					}
					g.writeEndArray();
					
					g.writeEndObject();
				}
			}
			
			g.writeEndArray();
				// ------------------------------------------------------
			
			// ------------------------------------------------------
			g.writeEndObject();
		}
		
	}
	
}
