package model;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import events.MetaDeviceAddedEvent;
import model.parametar.Parametar;
import model.parametar.ParametarFactory;
import utility.Constants;

public class Biblioteka {
	//Biblioteka je Singleton
	private HashMap<String, MetaPametniUredjaj> metaUredjaji;
	
	private static Biblioteka instance;
	
	private Biblioteka(){
		metaUredjaji = new HashMap<String,MetaPametniUredjaj>();
	}
	
	public static Biblioteka getInstance(){
		if(instance == null){
			instance = new Biblioteka();
			instance.load();
		}
		return instance;
	}

	public HashMap<String,MetaPametniUredjaj> getMetaUredjaji() {
		return metaUredjaji;
	}

	public void setMetaUredjaji(HashMap<String,MetaPametniUredjaj> metaUredjaji) {
		this.metaUredjaji = metaUredjaji;
	}
	
	public void dodajNoviMetaUredjaj(MetaPametniUredjaj m){
		metaUredjaji.put(m.getNaziv(), m);
		MetaDeviceAddedEvent e = new MetaDeviceAddedEvent(this, m);
		e.notifyListeners();
		save();
	}
	
	public void save(){
		System.out.println("Pokrenuto cuvanje biblioteke");
		//sacuvaj biblioteka.json fajl
		ObjectMapper mapper = new ObjectMapper();
		
		// Napraviti poseban nacin serijalizovanja, jer ne moze direktno da se
		// objekat Biblioteka upise u fajl.
		SimpleModule module = new SimpleModule();
		module.addSerializer(Biblioteka.class, new CustomBibliotekaSerializer());
		mapper.registerModule(module);
		 
		try {
			mapper.writeValue(new File(Constants.libraryPath), this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Zavrseno cuvanje biblioteke");
	}
	
	public void load(){
		//ucitaj biblioteka.json fajl
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = null;
		try {
			 root = mapper.readTree(new File(Constants.libraryPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
		JsonNode node = root.path("metaUredjaji");
		for(int i = 0; i < node.size(); i++){
			JsonNode currentMetaDevice = node.get(i);
			MetaPametniUredjaj m = new MetaPametniUredjaj();
			
			//ucitaj naziv i ikonicu(ako postoji)
			m.setNaziv(currentMetaDevice.path("naziv").asText());
			if(! currentMetaDevice.path("iconPath").isNull())
				m.setIcon(new ImageIcon(currentMetaDevice.path("iconPath").asText()));

			//ucitaj informaciju o tipovima parametara
			JsonNode currentParamList = currentMetaDevice.path("parametri");
			for(int j = 0; j < currentParamList.size(); j++){
				JsonNode currentParam = currentParamList.get(j);
				Parametar p = ParametarFactory.getNewParametar(currentParam.path("naziv").asText(), currentParam.path("tip").asText());
				m.getParametri().add(p);
			}
			
			//ucitaj informaciju o konfiguracijama
			JsonNode currentConfigList = currentMetaDevice.path("konfiguracije");
			for(int j = 0; j < currentConfigList.size(); j++){
				JsonNode currentConfig = currentConfigList.get(j);
				Konfiguracija k = new Konfiguracija();
				k.setNaziv(currentConfig.path("naziv").asText());
				JsonNode actualParamList = currentConfig.path("parametri");
				for(int r = 0; r < actualParamList.size(); r++){
					Parametar p = null;
					JsonNode param = actualParamList.get(r).path("podatak");
					try {
						p = m.getParametri().get(r).getClass().newInstance();
						p.setNaziv(m.getParametri().get(r).getNaziv());
						p.setValueFromJson(param);
					} catch (InstantiationException | IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e){
						System.out.println("Error in biblioteka.json file. Found mismatch between parameter types");
					}
					k.getParametri().add(p);
				}
				m.getKonfiguracije().add(k);
			}
			
			metaUredjaji.put(m.getNaziv(), m);
		}
		ispisi();
	}
	
	void ispisi(){
		for(Map.Entry<String, MetaPametniUredjaj> entry : metaUredjaji.entrySet()){
			System.out.println("Naziv meta uredjaja: " + entry.getKey());
			System.out.println("Parametri...");
			for(Parametar p : entry.getValue().getParametri()){
				System.out.println("Tip parametra: " + p.getClass().getName());
				System.out.println("Naziv parametra: " + p.getNaziv());
				//ne ispisuje se pridruzeni podatak posto je on null
			}
			System.out.println();
			System.out.println("Konfiguracije...");
			for(Konfiguracija k : entry.getValue().getKonfiguracije()){
				System.out.println("Naziv: " + k.getNaziv());
				System.out.println("Parametri u konfiguraciji...");
				for(Parametar p : k.getParametri()){
					System.out.println("Tip parametra: " + p.getClass().getName());
					System.out.println("Naziv parametra: " + p.getNaziv());
					System.out.println("Vrednost(NE treba da je NULL): " + p);
				}
				System.out.println();
			}
		}
	}
}
