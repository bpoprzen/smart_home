package model;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import model.parametar.Parametar;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

public class XmlStrategija implements Strategija {

	@Override
	public void serijalizuj(String putanja, PametniObjekat objekat) {
		try {
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		    Document doc = dBuilder.newDocument();
		        
		    Element rootElement = doc.createElement("pametniObjekat");
		    doc.appendChild(rootElement);
		    
		    for (PametniUredjaj p : objekat.getUredjaji()) {
		    	Element pamUredjaj = doc.createElement("pametniUredjaj");
		    	Attr attr = doc.createAttribute("naziv");
		    	attr.setValue(String.valueOf(p.getMetaUredjaj().getNaziv()));
		    	pamUredjaj.setAttributeNode(attr);
		    	
		    	for (Parametar param : p.getParametri()) {
		    		Element par = doc.createElement("parametar");
		    		Attr naziv = doc.createAttribute("naziv");
		    		naziv.setValue(param.getNaziv());
		    		par.setAttributeNode(naziv);
		    		
		    		par.appendChild(doc.createTextNode(String.valueOf(param.getPodatak())));
		    		
		    		pamUredjaj.appendChild(par);
		    	}
		    	
		    	rootElement.appendChild(pamUredjaj);
		    }
		    
		    TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformerFactory.newTransformer();
	        DOMSource source = new DOMSource(doc);
	        StreamResult result = new StreamResult(new File(putanja));
	        transformer.transform(source, result);
	         
	        // Output to console for testing
	        //StreamResult consoleResult = new StreamResult(System.out);
	        //transformer.transform(source, consoleResult);
			System.out.println("Pametni objekat sacuvan.");

	    } catch (Exception e) {
	    	System.out.println("Pametni objekat nije sacuvan.");
	        e.printStackTrace();
	         
	    }
	}

	@Override
	public void deserijalizuj(String putanja, PametniObjekat objekat) {
		File inputFile = new File(putanja);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
	        doc.getDocumentElement().normalize();
	        
	        NodeList nList = doc.getElementsByTagName("pametniUredjaj");
	        
	        // Ucitaj svaki pametni uredjaj
	        for (int temp = 0; temp < nList.getLength(); temp++) {
	        	PametniUredjaj pu = new PametniUredjaj();
	        	
	        	// Nadji odgovarajuci meta pam
	        	Element pamUredjaj = (Element) nList.item(temp);
	        	String naziv = pamUredjaj.getAttribute("naziv");
	        	MetaPametniUredjaj m = Biblioteka.getInstance().getMetaUredjaji().get(naziv);
	        	pu.setMetaUredjaj(m);
	        	
	        	// Za sve parametre iz xml-a nadji tip u meta pam uredjaju
	        	// i na osnovu tipa instanciraj param i dodaj ih u pam uredjaj
	        	NodeList paramList = pamUredjaj.getElementsByTagName("parametar");
	        	for (int i = 0; i < paramList.getLength(); i++) {
	        		Element param = (Element) paramList.item(i);
	        		Parametar newParam = null;
	        		for (Parametar temparam : m.getParametri()) {
	        			if (temparam.getNaziv().equals(param.getAttribute("naziv"))) {
	        				newParam = temparam.getDefaultCopy();
	        				break;
	        			}
	        		}
	        		if (newParam == null) {
	        			System.out.println("Nije pronadjen parametar.");
	        			continue;
	        		}
	        		String val = pamUredjaj.getElementsByTagName("parametar").item(i).getTextContent();
	        		newParam.setPodatak(val);
	        		
	        		pu.getParametri().add(newParam);
	        	}
	        	
	        	objekat.addUredjaj(pu);
	        }
	        System.out.println("Zavrseno ucitavanje");
		} catch (IOException | ParserConfigurationException | SAXException e) {
			//e.printStackTrace();
			System.err.print("File IO error: " + e.toString());
		}
	}

}
