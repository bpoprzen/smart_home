package model;

import events.CloseObjectEvent;
import events.CreateObjectDialogEvent;
import events.LoadObjectDialogEvent;
import events.LoadObjectEvent;
import events.SaveObjectDialogEvent;
import states.Inicijalno;
import states.StanjeAplikacije;

/**
 * Model komponenta. Mesto gde se Ä�uvaju podaci za operativnu upotrebu. Ne sme
 * da ima nikakve reference ka view komponenti. Model komponenta je u potpunosti
 * razdvojena od view komponenti koje vizualizuju model.
 *
 */
public class Aplikacija {

	// Atributi koji bi ovde trebalo da budu su biblioteka pametnih uredjaja
	// koji se cuvaju na disku, putanje do nedavno koriscenih pam. objekata,
	// pametni objekat koji se trenutno modifikuje u programu, model editora
	// (sa svojim stanjima)
	
	private Biblioteka biblioteka;
	private PametniObjekat ucitanObjekat;
	private StanjeAplikacije stanje;
	
	public Aplikacija(){
		biblioteka = Biblioteka.getInstance();
		stanje = new Inicijalno(this);
	}
	
	public StanjeAplikacije getStanje() {
		return stanje;
	}

	public void setStanje(StanjeAplikacije stanje) {
		this.stanje = stanje;
		this.stanje.entry();
	}
	
	public void kreiranjeZatrazeno(){
		this.stanje.kreiranjeZatrazeno();
	}
	
	public void ucitavanjeZatrazeno(){
		this.stanje.ucitavanjeZatrazeno();
	}
	
	public void zatvaranjeZatrazeno(){
		this.stanje.zatvaranjeZatrazeno();
	}
	
	public void cuvanjeZatrazeno(){
		this.stanje.cuvanjeZatrazeno();
	}
	
	public void exportZatrazen(){
		this.stanje.exportZatrazen();
	}
	
	public boolean kreiraj(){
		/*
		 * 1. Kreiraj dogadjaj koji zahteva od glavnog prozora izvrsavanje dijaloga za kreiranje pametne kuce
		 * 2. Obavesti listenere(samo glavni prozor - EditorView)
		 * 3. Kreiraj objekat(u skladu sa parametrima dobijenim iz dijaloga) i ucitaj ga
		 */
		// 1.
		CreateObjectDialogEvent event1 = new CreateObjectDialogEvent(this);
		
		//2.
		event1.notifyListeners();
		if(event1.getApsolutePath() != null){
			// 3.
			ucitanObjekat = new PametniObjekat(event1.getApsolutePath());
			// ucitanObjekat.ucitajSe();
			
			/*
			 * 4. Kreiraj dogadjaj koji znaci da je zavrseno ucitavanje objekta.
			 * 5. Obavesti gui komponente(ovde ih moze biti vise) da prikazu odgovarajuce podatke o ucitanom objektu
			 */
			LoadObjectEvent event = new LoadObjectEvent(this, ucitanObjekat);
			event.notifyListeners();
			return true;
			/*
			 * Razlog zbog kojeg postoje CreateObjectDialogEvent i LoadObjectEvent je
			 * da ne bi morali da pazimo kojim redom dodajemo listener-e.
			 * Na primer, da je postojao samo LoadObjectEvent, onda bi morali prvo da obavestimo EditorView da izvrsi
			 * dijalog, pa tek onda status bar da prikaze ucitan objekat. 
			 */
		}
		return false;
	}
	
	public boolean ucitaj(){
		/*
		 * 1. Kreiraj dogadjaj koji zahteva od glavnog prozora izvrsavanje dijaloga za ucitavanje pametne kuce
		 * 2. Obavesti listenere(samo glavni prozor - EditorView)
		 * 3. Kreiraj objekat(u skladu sa parametrima dobijenim iz dijaloga) i ucitaj ga
		 */
		LoadObjectDialogEvent event1 = new LoadObjectDialogEvent(this);
		event1.notifyListeners();
		if(event1.getApsolutePath() != null){
			ucitanObjekat = new PametniObjekat(event1.getApsolutePath());
			ucitanObjekat.ucitajSe();
			
			/*
			 * 4. Kreiraj dogadjaj da je zavrseno ucitavanje objekta.
			 * 5. Obavesti gui komponente(ovde ih moze biti vise) da prikazu odgovarajuce podatke o ucitanom objektu
			 */
			LoadObjectEvent event = new LoadObjectEvent(this, ucitanObjekat);
			event.notifyListeners();
			return true;
			/*
			 * Razlog zbog kojeg postoje LoadObjectDialogEvent i LoadObjectEvent je
			 * da ne bi morali da pazimo kojim redom dodajemo listener-e.
			 * Na primer, da je postojao samo LoadObjectEvent, onda bi morali prvo da obavestimo EditorView da izvrsi
			 * dijalog, pa tek onda status bar da prikaze ucitan objekat. 
			 */
		}
		return false;
		
	}
	
	public void zatvori(){
		/*
		 * 1.Kreiraj dogadjaj za zatvaranje pametnog objekta
		 * 2.Obavesti gui komponente da azuriraju svoje stanje
		 */
		CloseObjectEvent ev = new CloseObjectEvent(this);
		ev.notifyListeners();
	}
	
	public void sacuvaj(){
		/*
		 * 1. Kreiraj dogadjaj koji zahteva od glavnog prozora da pozove dijalog za cuvanje ucitanog pametnog objekta
		 * 2. U zavisnosti od rezultata dijaloga, izvrsi cuvanje
		 */
		SaveObjectDialogEvent e = new SaveObjectDialogEvent(this, ucitanObjekat.getNaziv());
		e.notifyListeners();
		if(e.isSaveRequested()){
			ucitanObjekat.sacuvajSe();
		}
	}
	
	public void export(){
		/*
		 * 1. Kreiraj dogadja koji trazi od glavnog prozora da se prikaze dijalog za cuvanje
		 * 2. Prevezi upravljac fajlovima na upravljac fajlovima koji odgovara parametrima odabranim u dijalogu
		 * 3. Exportuj
		 * 4. Vrati stari upravljac fajlovima
		 */
		
		// 1.
		CreateObjectDialogEvent event1 = new CreateObjectDialogEvent(this);
		event1.notifyListeners();
		if(event1.getApsolutePath() != null){
			// 2.
			String novaPutanja = event1.getApsolutePath();
			UpravljacFajlom temp =	ucitanObjekat.getFileManager();
			ucitanObjekat.setFileManager(new UpravljacFajlom(novaPutanja, ucitanObjekat,
					StrategyFactory.create(novaPutanja.split("\\.")[0])));
			// 3.
			ucitanObjekat.sacuvajSe();
			
			// 4.
			ucitanObjekat.setFileManager(temp);
		}
	}

	public void dodajUredjaj(PametniUredjaj pu){
		ucitanObjekat.dodajUredjaj(pu);
	}

	public PametniObjekat getUcitanObjekat() {
		return ucitanObjekat;
	}

	public void setUcitanObjekat(PametniObjekat ucitanObjekat) {
		this.ucitanObjekat = ucitanObjekat;
	}

	public Biblioteka getBiblioteka() {
		return biblioteka;
	}
}
