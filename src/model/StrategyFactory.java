package model;

public class StrategyFactory {
	public static Strategija create(String tip){
		if(tip.equals(".kuca")){
			return new RegularnaStrategija();
		}else if(tip.equals(".json")){
			return new JsonStrategija();
		}else{
			return new XmlStrategija();
		}
	}
}
