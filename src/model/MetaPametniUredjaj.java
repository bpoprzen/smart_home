package model;

import model.parametar.Parametar;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class MetaPametniUredjaj {
	private String naziv;
	private ArrayList<Konfiguracija> konfiguracije;
	private ArrayList<Parametar> parametri;
	private ImageIcon icon;
	public MetaPametniUredjaj(){
		this.konfiguracije = new ArrayList<Konfiguracija>();
		this.parametri = new ArrayList<Parametar>();
	}
	
	public MetaPametniUredjaj(String naziv, ArrayList<Konfiguracija> konfiguracije, ArrayList<Parametar> parametri, ImageIcon icon){
		this.naziv = naziv;
		this.konfiguracije = konfiguracije;
		this.parametri = parametri;
		this.icon = icon;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public ArrayList<Konfiguracija> getKonfiguracije() {
		return konfiguracije;
	}

	public void setKonfiguracije(ArrayList<Konfiguracija> konfiguracije) {
		this.konfiguracije = konfiguracije;
	}
	public void addKonfiguracija(Konfiguracija konf) { 
		konfiguracije.add(konf); 
		Biblioteka.getInstance().save();
	}
	public void updateKonfiguracija(Konfiguracija konf, ArrayList<Parametar> params)
	{
		int i = konfiguracije.indexOf(konf);
		konfiguracije.get(i).setParametri(params);
		Biblioteka.getInstance().save();
	}
	public void removeKonfiguracija(Konfiguracija konf){
		konfiguracije.remove(konf);
		Biblioteka.getInstance().save();
	}

	public ArrayList<Parametar> getParametri() {
		return parametri;
	}

	public void setParametri(ArrayList<Parametar> parametri) {
		this.parametri = parametri;
	}

	public ImageIcon getIcon() {
		return icon;
	}

	public void setIcon(ImageIcon icon) {
		this.icon = icon;
	}
	
	
	
}
