package model;

public class UpravljacFajlom {
	private String putanja;
	private PametniObjekat objekat;
	private Strategija strategija;
	
	public UpravljacFajlom(String putanja, PametniObjekat objekat, Strategija strategija){
		this.putanja = putanja;
		this.objekat = objekat;
		this.strategija = strategija;
	}
	
	void sacuvajObjekat(){
		this.strategija.serijalizuj(this.putanja, objekat);
	}
	
	void ucitajObjekat(){
		this.strategija.deserijalizuj(this.putanja, this.objekat);
	}

	public String getPutanja() {
		return putanja;
	}

	public void setPutanja(String putanja) {
		this.putanja = putanja;
	}

	public PametniObjekat getObjekat() {
		return objekat;
	}

	public void setObjekat(PametniObjekat objekat) {
		this.objekat = objekat;
	}

	public Strategija getStrategija() {
		return strategija;
	}

	public void setStrategija(Strategija strategija) {
		this.strategija = strategija;
	}
	
}
