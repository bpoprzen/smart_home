package model;

import model.parametar.Parametar;

import java.util.ArrayList;

public class PametniUredjaj {
	private MetaPametniUredjaj metaUredjaj;
	private ArrayList<Parametar> parametri;
	
	public PametniUredjaj(){
		parametri = new ArrayList<Parametar>();
	}
	
	public PametniUredjaj(MetaPametniUredjaj metaUredjaj){
		this.metaUredjaj = metaUredjaj;
		kopirajParametre(); //izvrsice kopiranje parametara u atribut parametri

	}

	public ArrayList<Parametar> getParametri() {
		return parametri;
	}
	public void setParametri(ArrayList<Parametar> parametri) {
		this.parametri = parametri;
	}

	public MetaPametniUredjaj getMetaUredjaj() {
		return metaUredjaj;
	}

	public void setMetaUredjaj(MetaPametniUredjaj metaUredjaj) {
		this.metaUredjaj = metaUredjaj;
	}
	
	void kopirajParametre(){
		//kopiraj parametre iz meta pametnog uredjaja u parametre this objekta
		this.parametri = new ArrayList<Parametar>();
		for(Parametar p : metaUredjaj.getParametri()){			
			Parametar kopija = p.getDefaultCopy();
			this.parametri.add(kopija);
		}
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PametniUredjaj)) {
			return false;
		}
		PametniUredjaj pamUredjaj2 = (PametniUredjaj) obj;
		if (!this.metaUredjaj.getNaziv().equals(pamUredjaj2.getMetaUredjaj().getNaziv())) {
			return false;
		}
		
		return true;
	}
}
