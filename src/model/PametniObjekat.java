package model;

import java.io.File;
import java.util.ArrayList;

public class PametniObjekat {
	private String naziv;
	private UpravljacFajlom fileManager;
	private ArrayList<PametniUredjaj> uredjaji;
	
	public PametniObjekat(String putanja){
		String arr[] = new File(putanja).getName().split("\\.");
		this.naziv = arr[0]; //precitko zar ne :(
		System.out.println(this.naziv);
		this.fileManager = new UpravljacFajlom(putanja, this, StrategyFactory.create(arr[1]));
		this.uredjaji = new ArrayList<PametniUredjaj>();
	}

	public void dodajUredjaj(PametniUredjaj pu){
		uredjaji.add(pu);
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public ArrayList<PametniUredjaj> getUredjaji() {
		return uredjaji;
	}
	public void setUredjaji(ArrayList<PametniUredjaj> uredjaji) {
		this.uredjaji = uredjaji;
	}
	public UpravljacFajlom getFileManager() {
		return fileManager;
	}
	public void setFileManager(UpravljacFajlom fileManager) {
		this.fileManager = fileManager;
	}
	public void ucitajSe(){
		this.fileManager.ucitajObjekat();
	}
	public void sacuvajSe(){
		this.fileManager.sacuvajObjekat();
	}
	
	public void addUredjaj(PametniUredjaj uredjaj) {
		this.uredjaji.add(uredjaj);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof PametniObjekat)) {
			return false;
		}
		PametniObjekat pamObj2 = (PametniObjekat) o;
		if (!this.naziv.equals(pamObj2.getNaziv())){
			return false;
		}
		if (!this.uredjaji.equals(pamObj2.getUredjaji())) {
			return false;
		}
		return true;
		
	}
	
}
