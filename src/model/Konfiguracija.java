package model;

import model.parametar.Parametar;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Konfiguracija {
	private String naziv;
	private ArrayList<Parametar> parametri;
	
	public Konfiguracija(){
		parametri = new ArrayList<Parametar>();
	}
	public Konfiguracija(String ime, ArrayList<Parametar> params) {
		naziv = ime;
		parametri = kopiraj(params);
	}
	
	ArrayList<Parametar> kopiraj(ArrayList<Parametar> params){
		ArrayList<Parametar> ret = new ArrayList<Parametar>();
		for(Parametar p : params){
			ret.add(p.kopiraj());
		}
		return ret;
	}
	
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	public ArrayList<Parametar> getParametri() {
		return parametri;
	}
	public void setParametri(ArrayList<Parametar> parametri) {
		this.parametri = parametri;
	}
	
	
}
