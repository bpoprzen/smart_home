package model.parametar;

import com.fasterxml.jackson.databind.JsonNode;

public class BinarniParametar extends Parametar {
	private Boolean podatak;
	
	public BinarniParametar(){
		super();
	}
	
	public BinarniParametar(String naziv, Boolean podatak) {
		super(naziv);
		this.podatak = podatak;
	}

	@Override
	public Boolean getPodatak() {
		return podatak;
	}

	@Override
	public void setPodatak(Object o) {
		if (o.getClass() == String.class) {
			this.podatak = Boolean.parseBoolean((String) o);
		}
		else {
			this.podatak = (Boolean) o;
		}
	}

	@Override
	public String toString() {
		return podatak.toString();
	}

	@Override
	public Parametar getDefaultCopy() {
		//prilicno sam siguran da je ovo duboka kopija, String je immutable pa naziv ne mora da se kopira
		BinarniParametar copy = new BinarniParametar(this.getNaziv(), new Boolean(false));
		return copy;
	}

	@Override
	public void setValueFromJson(JsonNode node) {
		this.podatak = node.asBoolean();	
	}
	
	@Override
	public String getTip() {
		return "binarni";
	}

	@Override
	public Parametar kopiraj() {
		return new BinarniParametar(this.getNaziv(), new Boolean(this.getPodatak()));
	}
	
	
}
