package model.parametar;

import com.fasterxml.jackson.databind.JsonNode;

public abstract class Parametar {
	private String naziv;
	
	public Parametar(){}
	
	public Parametar(String naziv) {
		this.naziv = naziv;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public abstract void setValueFromJson(JsonNode node);
	public abstract Parametar getDefaultCopy();
	
	@Override
	public String toString() {
		return "";
	}

	public abstract Object getPodatak(); 
	public abstract void setPodatak(Object o);
	
	public String getTip() {
		return "apstraktni";
	}

	public abstract Parametar kopiraj();

	
	
}
