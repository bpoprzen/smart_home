package model.parametar;

import com.fasterxml.jackson.databind.JsonNode;

public class RealniParametar extends Parametar {
	private Double podatak;
	
	public RealniParametar(){
		super();
	}
	
	public RealniParametar(String naziv, Double podatak) {
		super(naziv);
		this.podatak = podatak;
	}
	
	@Override
	public Double getPodatak() {
		return podatak;
	}
	
	@Override
	public void setPodatak(Object o) {
		if (o.getClass() == String.class) {
			this.podatak = Double.valueOf((String) o);
		}
		else {
			this.podatak = (Double) o;
		}
	}
	@Override
	public String toString() {
		return podatak.toString();
	}
	@Override
	public Parametar getDefaultCopy() {
		//prilicno sam siguran da je ovo duboka kopija, String je immutable pa naziv ne mora da se kopira
		RealniParametar copy = new RealniParametar(this.getNaziv(), new Double(0));
		return copy;
	}
	@Override
	public void setValueFromJson(JsonNode node) {
		this.podatak = node.asDouble();
	}
	
	@Override
	public String getTip() {
		return "realni";
	}

	@Override
	public Parametar kopiraj() {
		return new RealniParametar(this.getNaziv(), new Double(this.getPodatak().doubleValue()));
	}

	

}
