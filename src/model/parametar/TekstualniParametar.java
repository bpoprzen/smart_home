package model.parametar;

import com.fasterxml.jackson.databind.JsonNode;

public class TekstualniParametar extends Parametar {
	private String podatak;

	public TekstualniParametar(){
		super();
	}
	
	public TekstualniParametar(String naziv, String podatak) {
		super(naziv);
		this.podatak = podatak;
	}
	
	@Override
	public String getPodatak() {
		return podatak;
	}

	@Override
	public void setPodatak(Object o) {
		this.podatak = (String) o;
	}

	@Override
	public String toString() {
		return podatak;
	}

	@Override
	public Parametar getDefaultCopy() {
		//prilicno sam siguran da je ovo duboka kopija, String je immutable pa naziv ne mora da se kopira
		TekstualniParametar copy = new TekstualniParametar(this.getNaziv(), "");
		return copy;
	}

	@Override
	public void setValueFromJson(JsonNode node) {
		this.podatak = node.asText();
	}
	
	@Override
	public String getTip() {
		return "tekstualni";
	}

	@Override
	public Parametar kopiraj() {
		return new TekstualniParametar(this.getNaziv(), new String(this.getPodatak().toCharArray()));
	}

	
	
}
