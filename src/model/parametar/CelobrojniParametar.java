package model.parametar;

import com.fasterxml.jackson.databind.JsonNode;

public class CelobrojniParametar extends Parametar {
	private Integer podatak;
	
	public CelobrojniParametar(){
		super();
	}
	
	public CelobrojniParametar(String naziv, Integer podatak) {
		super(naziv);
		this.podatak = podatak;
	}
	@Override
	public Integer getPodatak() {
		return podatak;
	}
	
	@Override
	public void setPodatak(Object o) {
		if (o.getClass() == String.class) {
			this.podatak = Integer.parseInt((String) o);
		}
		else {
			this.podatak = (Integer) o;
		}
	}
	@Override
	public String toString() {
		return podatak.toString();
	}
	@Override
	public Parametar getDefaultCopy() {
		//prilicno sam siguran da je ovo duboka kopija, String je immutable pa naziv ne mora da se kopira
		CelobrojniParametar copy = new CelobrojniParametar(this.getNaziv(), new Integer(0));
		return copy;
	}
	@Override
	public void setValueFromJson(JsonNode node) {
		this.podatak = node.asInt();
	}

	@Override
	public String getTip() {
		return "celobrojni";
	}

	@Override
	public Parametar kopiraj() {
		return new CelobrojniParametar(this.getNaziv(), new Integer(this.getPodatak().intValue()));
	}

	
}
