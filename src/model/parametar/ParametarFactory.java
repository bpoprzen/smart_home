package model.parametar;

public class ParametarFactory {
	public static Parametar getNewParametar(String naziv, String tip){
		if(tip.equals("binarni")){
			return new BinarniParametar(naziv, null);
		}else if(tip.equals("tekstualni")){
			return new TekstualniParametar(naziv, null);
		}else if(tip.equals("celobrojni")){
			return new CelobrojniParametar(naziv, null);
		}else{
			return new RealniParametar(naziv, null);
		}
	}
}
