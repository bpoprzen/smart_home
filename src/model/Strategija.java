package model;

public interface Strategija {
	void serijalizuj(String putanja, PametniObjekat objekat);
	void deserijalizuj(String putanja, PametniObjekat objekat);
}
