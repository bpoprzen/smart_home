package tests;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import events.LoadObjectEvent;
import model.Aplikacija;
import model.Biblioteka;
import model.MetaPametniUredjaj;
import model.PametniObjekat;
import model.PametniUredjaj;
import view.EditorView;

public class TestPrikazObjekta {
	Aplikacija app;
	PametniObjekat pametniObjekat;
	String path = "mySmartHomeTest.xml";
	EditorView ev;
	

	@Before
	public void setUp() throws Exception {
		app = new Aplikacija();
		
		ev = new EditorView(app, null);
				
		pametniObjekat = new PametniObjekat(path);
		
		Iterator<MetaPametniUredjaj> iter = Biblioteka.getInstance().getMetaUredjaji().values().iterator();
		PametniUredjaj pamUredjaj1 = new PametniUredjaj(iter.next());
		pametniObjekat.addUredjaj(pamUredjaj1);
		PametniUredjaj pamUredjaj2 = new PametniUredjaj(iter.next());
		pametniObjekat.addUredjaj(pamUredjaj2);
		app.setUcitanObjekat(pametniObjekat);
	}

	@Test
	public void test() {		
		LoadObjectEvent event = new LoadObjectEvent(this, pametniObjekat);
		event.notifyListeners();
		
		// Objekat je prikazan na Status baru
		assertEquals(ev.getStatusBar().getStatusLabel().getText(), pametniObjekat.getNaziv());
		
		// Objekat je prikazan na Blueprint-u
		assertEquals(ev.getBlueprintPanel().getComponents().length, pametniObjekat.getUredjaji().size());
	}
}
