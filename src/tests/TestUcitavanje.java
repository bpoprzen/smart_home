package tests;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.Aplikacija;
import model.Biblioteka;
import model.MetaPametniUredjaj;
import model.PametniObjekat;
import model.PametniUredjaj;

/**
 * 
 * Klasa koja predstavlja test cuvanja i ucitavanja pametnog objekta.
 * Kreira se pametan objekat Original, koji se sacuva. Potom se pametni
 * objekat Kopija ucita sa putanje na kojoj se sacuvan Original. Testira se
 * poklapanje pametnih objekata Original i Kopija.
 *
 */
public class TestUcitavanje {
	Aplikacija app;
	PametniObjekat pametniObjekat;
	String path = "mySmartHomeTest.xml";

	@Before
	public void setUp() throws Exception {
		app = new Aplikacija();
				
		pametniObjekat = new PametniObjekat(path);
		
		Iterator<MetaPametniUredjaj> iter = Biblioteka.getInstance().getMetaUredjaji().values().iterator();
		PametniUredjaj pamUredjaj1 = new PametniUredjaj(iter.next());
		pametniObjekat.addUredjaj(pamUredjaj1);
		PametniUredjaj pamUredjaj2 = new PametniUredjaj(iter.next());
		pametniObjekat.addUredjaj(pamUredjaj2);
		
		app.setUcitanObjekat(pametniObjekat);
		
		pametniObjekat.sacuvajSe();
	}

	@Test
	public void test() {		
		PametniObjekat ucitanObjekat = new PametniObjekat(path);
		ucitanObjekat.ucitajSe();
		
		assertEquals(pametniObjekat, ucitanObjekat);
	}

}
