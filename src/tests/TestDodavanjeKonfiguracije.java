package tests;

import model.*;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestDodavanjeKonfiguracije {
    MetaPametniUredjaj mpu;

    @Before
    public void setUp() throws Exception {
        mpu = new MetaPametniUredjaj();
    }

    @Test
    public void test() {
        Konfiguracija konf = new Konfiguracija();
        mpu.addKonfiguracija(konf);
        boolean postoji = mpu.getKonfiguracije().contains(konf);

        assertTrue(postoji);
    }
}
