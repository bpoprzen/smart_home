package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model.Aplikacija;
import model.Biblioteka;
import model.MetaPametniUredjaj;

public class TestDodavanjeMetaUredjaja {
	Aplikacija app;
	int sizeBefore;
	
	@Before
	public void setUp() throws Exception {
		app = new Aplikacija(); //load() se poziva u u getInstance() od Biblioteke
		sizeBefore = Biblioteka.getInstance().getMetaUredjaji().size();
		Biblioteka.getInstance().dodajNoviMetaUredjaj(new MetaPametniUredjaj("TestUredjaj", new ArrayList<>(), new ArrayList<>(), null));
	}
	
	@Test
	public void test() {
		Biblioteka.getInstance().load();
		int currentSize = Biblioteka.getInstance().getMetaUredjaji().size();
		assertEquals(sizeBefore + 1, currentSize);
	}

}
