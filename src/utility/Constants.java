package utility;

public class Constants {
	public static final String libraryPath = "src" + System.getProperty("file.separator") + "biblioteka.json";
	public static final String userDefImagePath = "src" + System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "korisnickiDef.png";
}
