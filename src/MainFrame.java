import javax.swing.JFrame;

import model.Aplikacija;
import utility.IsKeyDown;
import view.EditorView;

/**
 *
 */
public class MainFrame extends JFrame {

    public static final String APP_TITLE = "Smart Home";

    public MainFrame() {
        /*
        * Inicijalizuj podatke utility klasa
        *
        * */
        IsKeyDown.Init();
        //UredjajInfo.SetValues();

        /*
        * Postavi parametre glavnog prozora
        * */
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle(APP_TITLE);
        setSize(600, 400);
        setLocationRelativeTo(null);


        /*
        Instanciraj model - ucitaj bibilioteku sacuvanih pametnih uredjaja sa
        diska, ucitaj putanje do nedavno koriscenih pametnih objekata
        */
        Aplikacija app = new Aplikacija();

        /*
        Instaciraj view - prikazi ucitane pametne uredjaje i cekaj da korisnik
        odabere neki pametan objekat i potom ga opsluzuj. Ponasanje view-a 
        (grafickog editora) je modelovano state patternom.
        */
        EditorView ev = new EditorView(app, this);

        add(ev);

    }

    public static void main(String[] args) {
    	System.out.println(System.getProperty("user.dir"));
        new MainFrame().setVisible(true);
    }
}