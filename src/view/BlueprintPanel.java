package view;

import events.CloseObjectEvent;
import events.CloseObjectEventListener;
import events.LoadObjectEvent;
import events.LoadObjectEventListener;
import model.PametniUredjaj;

import javax.swing.*;
import java.awt.*;

public class BlueprintPanel extends JPanel implements CloseObjectEventListener, LoadObjectEventListener {
    private EditorView parent;
    private Image bgImage;

    public BlueprintPanel(ImageIcon bg, EditorView p){
        setLayout(null);
        bgImage = bg.getImage();
        parent = p;

        setVisible(true);
    }

    public void prikaziUredjaj(PametniUredjaj pu) {
        /*
        * Napravi view za pametni uredjaj i doda ga na tlocrt panel (ovo)
        * */

        //Napravi mu jedan view i vezi za dogadjaje
        ImageIcon icon = pu.getMetaUredjaj().getIcon();
        PametniUredjajView puv = new PametniUredjajView(pu, icon, 60, 60, parent);

        //Dodaj na prozor
        add(puv);

        //Osvezi prikaz
        revalidate();
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);
        g.drawImage(bgImage, 0, 0, null);
    }

    @Override
    public void eventPerformed(LoadObjectEvent event) {
        //kada se obavi ucitavanje objekta, prikazi bp panel

        //dodaj po view za svaki pametni uredjaj u objektu
        for (PametniUredjaj pu : parent.getApp().getUcitanObjekat().getUredjaji()) {
            prikaziUredjaj(pu);
        }

        parent.add(this);
        setVisible(true);
        revalidate();
        repaint();

    }
    @Override
    public void eventPerformed(CloseObjectEvent event) {
        //kada se obavi zatvaranje objekta, zatvori bp panel
    	this.removeAll();
        setVisible(false);
        parent.remove(this);
    }
}
