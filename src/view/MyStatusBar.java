package view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import events.CloseObjectEvent;
import events.CloseObjectEventListener;
import events.LoadObjectEvent;
import events.LoadObjectEventListener;
import model.PametniObjekat;

public class MyStatusBar extends JPanel implements LoadObjectEventListener, CloseObjectEventListener{
	EditorView parent;
	JLabel statusLabel;
	public MyStatusBar(EditorView parent){
		setBorder(new BevelBorder(BevelBorder.LOWERED));
		parent.add(this, BorderLayout.SOUTH);
		this.parent = parent;
		this.setPreferredSize(new Dimension(parent.getWidth(), 25));
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.statusLabel = new JLabel("Initial application state.");
		this.statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
		this.add(statusLabel);
	}

	@Override
	public void eventPerformed(LoadObjectEvent event) {
		PametniObjekat obj = event.getUcitanObjekat();
		this.statusLabel.setText(obj.getNaziv());
	}

	@Override
	public void eventPerformed(CloseObjectEvent event) {
		this.statusLabel.setText("Initial application state");
	}

	public JLabel getStatusLabel() {
		return statusLabel;
	}

	public void setStatusLabel(JLabel statusLabel) {
		this.statusLabel = statusLabel;
	}
	
	
}
