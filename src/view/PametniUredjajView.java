package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import events.DragListener;
import model.PametniUredjaj;

public class PametniUredjajView extends JButton{

    private PametniUredjaj pametniUredjaj;
    EditorView grandParent;

    public PametniUredjajView(PametniUredjaj pu, ImageIcon icon, int width, int height, EditorView grandParent)
    {
        super(icon);

        pametniUredjaj = pu;
        this.grandParent = grandParent;
        
        // Resize icon to fit button
        if(pu.getMetaUredjaj().getIcon() != null){
	        Image img = icon.getImage();
	        Image resizedImage = img.getScaledInstance(width, height,  Image.SCALE_SMOOTH);
	        setIcon(new ImageIcon(resizedImage));
        }else{
        	setText(pu.getMetaUredjaj().getNaziv());
        }
        setSize(width, height);
        DragListener drag = new DragListener();
        addMouseListener(drag);
        addMouseMotionListener(drag);
    	
        PametniUredjajView pv = this;
        addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                if(e.getClickCount()==2){
                	pv.grandParent.deviceDoubleClicked(pv);
                }
            }
        });
    }

	public PametniUredjaj getPametniUredjaj() {
		return pametniUredjaj;
	}
    
    


}
