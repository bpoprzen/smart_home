package view.parametar;

import model.parametar.BinarniParametar;
import model.parametar.CelobrojniParametar;
import model.parametar.Parametar;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BinarniParamView extends ParametarView {
    public JLabel lbl_name;
    public JCheckBox check;
    public BinarniParametar binParam;

    public BinarniParamView(BinarniParametar bp) {
        setLayout(new GridLayout(1, 2, 20, 1));
        binParam = bp;

        lbl_name = new JLabel(bp.getNaziv());
        check = new JCheckBox();
        check.setSelected(bp.getPodatak());


        add(lbl_name);
        add(check);
    }

    public void PushToModel() {
        binParam.setPodatak(check.isSelected());
    }
    
    @Override
	public void kopiraj(Parametar parametar) {
		this.check.setSelected(((BinarniParametar)parametar).getPodatak());
	}

}
