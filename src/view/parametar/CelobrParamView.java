package view.parametar;

import model.parametar.CelobrojniParametar;
import model.parametar.Parametar;
import model.parametar.RealniParametar;

import javax.swing.*;
import java.awt.*;

public class CelobrParamView extends ParametarView {
    public JLabel lbl_name;
    public JTextField txt_value;
    public CelobrojniParametar celobrParam;

    public CelobrParamView(CelobrojniParametar cp) {
        setLayout(new GridLayout(1, 2, 20, 1));
        celobrParam = cp;

        lbl_name = new JLabel(cp.getNaziv());
        txt_value = new JTextField(cp.getPodatak().toString());

        add(lbl_name);
        add(txt_value);
    }

    public void PushToModel() {
        celobrParam.setPodatak(Integer.parseInt(txt_value.getText()));
    }
    
    @Override
	public void kopiraj(Parametar parametar) {
		this.txt_value.setText(((CelobrojniParametar)parametar).getPodatak().toString());		
	}
}
