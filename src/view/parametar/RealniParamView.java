package view.parametar;

import model.parametar.Parametar;
import model.parametar.RealniParametar;

import javax.swing.*;
import java.awt.*;

public class RealniParamView extends ParametarView {
    public JLabel lbl_name;
    public JTextField txt_value;
    public RealniParametar realParam;

    public RealniParamView(RealniParametar rp) {
        setLayout(new GridLayout(1, 2, 20, 1));
        realParam = rp;

        lbl_name = new JLabel(rp.getNaziv());
        txt_value = new JTextField(rp.getPodatak().toString());

        add(lbl_name);
        add(txt_value);
    }

    public void PushToModel() {
        realParam.setPodatak(Double.parseDouble(txt_value.getText()));
    }
    
    @Override
	public void kopiraj(Parametar parametar) {
		this.txt_value.setText(((RealniParametar)parametar).getPodatak().toString());		
	}
}
