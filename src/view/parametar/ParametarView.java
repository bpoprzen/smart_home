package view.parametar;

import javax.swing.*;

import model.parametar.Parametar;

public abstract class ParametarView extends JPanel {
    public abstract void PushToModel();
	public abstract void kopiraj(Parametar parametar);

}
