package view.parametar;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;

import model.parametar.Parametar;
import model.parametar.TekstualniParametar;


public class TekstualniParamView extends ParametarView {
	public JLabel lbl_name;
    public JTextField txt_value;
    public TekstualniParametar tekstualniParam;

    public TekstualniParamView(TekstualniParametar tp) {
        setLayout(new GridLayout(1, 2));
        tekstualniParam = tp;

        lbl_name = new JLabel(tp.getNaziv());
        txt_value = new JTextField(tp.getPodatak().toString());

        add(lbl_name);
        add(txt_value);
    }

    @Override
    public void PushToModel() {
        tekstualniParam.setPodatak(txt_value.getText());
    }

    
    @Override
	public void kopiraj(Parametar parametar) {
		this.txt_value.setText(((TekstualniParametar)parametar).getPodatak());		
	}
}
