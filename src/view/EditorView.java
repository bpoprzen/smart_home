package view;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import events.CloseObjectEvent;
import events.CloseObjectEventListener;
import events.CreateObjectDialogEvent;
import events.CreateObjectDialogEventListener;
import events.LoadObjectDialogEvent;
import events.LoadObjectDialogEventListener;
import events.LoadObjectEvent;
import events.MetaDeviceAddedEvent;
import events.SaveObjectDialogEvent;
import events.SaveObjectDialogEventListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Aplikacija;
import model.Biblioteka;
import model.PametniUredjaj;

/**
 * View komponenta
 * Od atributa bi trebalo da sadrzi glavne GUI elemente menubar, panel sa tlocrtima
 * i tulbar
 */
public class EditorView extends JPanel implements CreateObjectDialogEventListener, LoadObjectDialogEventListener, SaveObjectDialogEventListener{
    private Aplikacija app; // model
	private JFrame frame;
    private JMenuBar menuBar;

    private JMenu fileMenu;
    private JMenuItem newOption;
    private JMenuItem openOption;
    private JMenuItem closeOption;
    private JMenuItem saveOption;
    private JMenuItem exportOption;
    
    private JMenu editMenu;
    
    MyStatusBar statusBar;
    MyToolBar toolBar;
    BlueprintPanel blueprintPanel;
    
    public EditorView(Aplikacija app, JFrame par){
        setLayout(new BorderLayout());
        frame = par;
        setApp(app);

        initGUI();
        constructGUI();


        addListeners(); //dodaje anonimne klase koje osluskuju dogadjaje nad GUI komponentama
        subscribeListeners(); //dodaje GUI komponente kao listenere klasa modela
        
    }

    private void initGUI(){
        menuBar = new JMenuBar();
        fileMenu = new JMenu("File");
        newOption = new JMenuItem("New house");
        openOption = new JMenuItem("Open house");
        closeOption = new JMenuItem("Close house");
        saveOption = new JMenuItem("Save changes");
        exportOption = new JMenuItem("Export house");
        editMenu = new JMenu("Edit");
        statusBar = new MyStatusBar(this);
        toolBar = new MyToolBar(this);
        blueprintPanel = new BlueprintPanel(new ImageIcon("src/images/blueprint.jpg"), this);
    }

    private void constructGUI(){
    	//status bar i tool bar se ne dodaju jos uvek, njima se prosledio parent da bi se kasnije dodali
        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        fileMenu.add(newOption);
        fileMenu.add(openOption);
        fileMenu.add(closeOption);
        fileMenu.add(saveOption);
        fileMenu.add(exportOption);
        add(menuBar, BorderLayout.NORTH);
    }

    private void addListeners(){
    	newOption.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				app.kreiranjeZatrazeno();
			}
		});
    	
    	openOption.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				app.ucitavanjeZatrazeno();
			}
		});
    	
    	closeOption.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				app.zatvaranjeZatrazeno();
			}
		});
    	
    	saveOption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				app.cuvanjeZatrazeno();
			}
		});
    	
    	exportOption.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				app.exportZatrazen();
			}
		});
    	
    }
    
    public void subscribeListeners(){
    	//Dodaj EditorView kao osluskivac svih dogadjaja koji zahtevaju akciju glavnog prozora
    	CreateObjectDialogEvent.addListeners(this);
    	LoadObjectDialogEvent.addListeners(this);
    	SaveObjectDialogEvent.addListeners(this);
    	
    	//Dodaj status bar kao osluskivac svih dogadjaja koji zahtevaju akciju status bara
    	LoadObjectEvent.addListeners(statusBar);
    	CloseObjectEvent.addListeners(statusBar);
    	
    	//Dodaj tool bar kao osluskivac svih dogadjaja koji zahtevaju akciju tool bara
    	LoadObjectEvent.addListeners(toolBar);
    	CloseObjectEvent.addListeners(toolBar);
    	MetaDeviceAddedEvent.addListeners(toolBar);

    	//Dodaj blueprint panel kao osluskivac svih dogadjaja koji zahtevaju akciju bp panela
		LoadObjectEvent.addListeners(blueprintPanel);
		CloseObjectEvent.addListeners(blueprintPanel);

    	//TODO obraditi ostale gui komponente
    }
    
    public Aplikacija getApp() {
        return app;
    }

    public void setApp(Aplikacija app) {
        this.app = app;
    }

    public JFrame getFrame() {
		return frame;
	}
	@Override
	public void eventPerformed(SaveObjectDialogEvent event) {
		//Otvori dijalog za cuvanje
		int result = JOptionPane.showConfirmDialog(this, "Would you like to save changes to object " + event.getNazivObjekta() + "?");
		event.setSaveRequested(result == JOptionPane.YES_OPTION);
	}

	@Override
	public void eventPerformed(LoadObjectDialogEvent e) {
		//Otvori dijalog za odabir objekta za ucitavanje
		JFileChooser dialog = new JFileChooser();
		dialog.setAcceptAllFileFilterUsed(false);
		dialog.addChoosableFileFilter(new FileNameExtensionFilter("KUCA file", "kuca"));
		dialog.addChoosableFileFilter(new FileNameExtensionFilter("JSON file", "json"));
		dialog.addChoosableFileFilter(new FileNameExtensionFilter("XML file", "xml"));
		int retval = dialog.showOpenDialog(this);
		if(retval == JFileChooser.APPROVE_OPTION){
			e.setApsolutePath(dialog.getSelectedFile().getAbsolutePath());
		}
	}

	@Override
	public void eventPerformed(CreateObjectDialogEvent e) {
		//Otvori dijalog za kreiranje objekta
		CreateFileDialog dialog = new CreateFileDialog(this);
		String apsolutePath = dialog.run();
		e.setApsolutePath(apsolutePath);		
	}

	public void dodajUredjaj(String naziv) {
		//Dodaj pametan uredjaj u model
		PametniUredjaj pu = new PametniUredjaj(Biblioteka.getInstance().getMetaUredjaji().get(naziv));
		app.dodajUredjaj(pu);

		// Dodaj view na panelu
		blueprintPanel.prikaziUredjaj(pu);
	}
	
	public void deviceDoubleClicked(PametniUredjajView puv){
		PametniUredjajDialog pud = new PametniUredjajDialog(this, puv);
		pud.run();
	}
	
	public void dodajKorisnickiDefinisaniUredjaj(){
		//dijalog za definisanje novog korisnicki definisanog uredjaja(meta pametnog uredjaja)
		CreateDeviceTypeDialog cd = new CreateDeviceTypeDialog(this);
		boolean retval = cd.run();
		if(retval){
			//dodaj u biblioteku
			//biblioteka ce notificirati tulbar da se azurira
			this.app.getBiblioteka().dodajNoviMetaUredjaj(cd.getDevice());
		}
	}

	public MyStatusBar getStatusBar() {
		return statusBar;
	}

	public void setStatusBar(MyStatusBar statusBar) {
		this.statusBar = statusBar;
	}

	public MyToolBar getToolBar() {
		return toolBar;
	}

	public void setToolBar(MyToolBar toolBar) {
		this.toolBar = toolBar;
	}

	public BlueprintPanel getBlueprintPanel() {
		return blueprintPanel;
	}

	public void setBlueprintPanel(BlueprintPanel blueprintPanel) {
		this.blueprintPanel = blueprintPanel;
	}
	
	
}
