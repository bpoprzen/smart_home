package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;


import events.CloseObjectEvent;
import events.CloseObjectEventListener;
import events.LoadObjectEvent;
import events.LoadObjectEventListener;
import events.MetaDeviceAddedEvent;
import events.MetaDeviceAddedEventListeners;
import model.Biblioteka;
import model.MetaPametniUredjaj;

public class MyToolBar extends JToolBar implements LoadObjectEventListener, CloseObjectEventListener, MetaDeviceAddedEventListeners{
	private ArrayList<JButton> buttons;
	private JButton btnKorisnickiDef;
	private JScrollPane scroll;
	private JPanel p;
	private EditorView parent;
	
	public MyToolBar(EditorView parent){
		this.parent = parent;
		buttons = new ArrayList<JButton>();
		for(Map.Entry<String, MetaPametniUredjaj> entry : Biblioteka.getInstance().getMetaUredjaji().entrySet()){
			JButton b = null;
			if(entry.getValue().getIcon() != null)
				//predefinisani meta urdjaji imaju ikonice
				b = new JButton(entry.getValue().getIcon());
			else
				//korisnicki definisani meta uredjaji nemaju ikonice
				b = new JButton(entry.getValue().getNaziv());
			b.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					parent.dodajUredjaj(entry.getKey());
	            }
	        });
			buttons.add(b);
		}
		this.btnKorisnickiDef = new JButton(new ImageIcon(utility.Constants.userDefImagePath));
        this.btnKorisnickiDef.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
				parent.dodajKorisnickiDefinisaniUredjaj();
            }
        });
        
        	
	}
	
	@Override
	public void eventPerformed(LoadObjectEvent event) {
		//kada se obavi ucitavanje objekta, prikazi toolbar
		panelInit();
		
	}
	private void panelInit() {
		p = new JPanel(new GridLayout(this.buttons.size() + 1, 1));
		for(JButton b : buttons){
			p.add(b);
			
		}
		p.add(btnKorisnickiDef);
		//resizeovane su sve ikonice u images rucno
		//takodje je dodat scrollbar kako ne bi bilo moguce videti sve objekte koji su definisani
		scroll = new JScrollPane();
		scroll.setViewportView(p);
		scroll.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		
        this.add(scroll);
        
        this.setOrientation(VERTICAL);
		this.setFloatable(false);
		this.parent.add(this, BorderLayout.EAST);
		p.revalidate();
		this.setVisible(true);
	}

	private void panelDestruct(){
		this.scroll.remove(p);
		this.remove(scroll);
		this.setVisible(false);
		this.parent.remove(this);
	}
	
	@Override
	public void eventPerformed(CloseObjectEvent event) {
		//kada se obavi zatvaranje objekta, zatvori toolbar
		panelDestruct();
			
	}
	@Override
	public void eventPerformed(MetaDeviceAddedEvent e) {
		JButton b = new JButton(e.getDevice().getNaziv());
		b.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				parent.dodajUredjaj(e.getDevice().getNaziv());
			}
		});
		buttons.add(b);

		panelDestruct();
		panelInit();
	}
}
