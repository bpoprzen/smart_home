package view;

import model.parametar.Parametar;

public class ParametarDropdownView {
	private Parametar parametar;
	
	ParametarDropdownView(Parametar parametar){
		this.parametar = parametar;
	}

	public Parametar getParametar() {
		return parametar;
	}

	public void setParametar(Parametar parametar) {
		this.parametar = parametar;
	}

	@Override
	public String toString() {
		return parametar.getNaziv() + " (" + parametar.getClass().getSimpleName() + ")";
	}
	
	

}
