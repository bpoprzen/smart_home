package view;

import model.Konfiguracija;
import model.PametniUredjaj;
import model.parametar.BinarniParametar;
import model.parametar.CelobrojniParametar;
import model.parametar.Parametar;
import model.parametar.RealniParametar;
import model.parametar.TekstualniParametar;
import view.parametar.BinarniParamView;
import view.parametar.CelobrParamView;
import view.parametar.ParametarView;
import view.parametar.RealniParamView;
import view.parametar.TekstualniParamView;

import javax.swing.*;

import org.hamcrest.core.IsInstanceOf;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

public class PametniUredjajDialog extends JDialog {
    EditorView parent;
    PametniUredjajView puv;
    JButton btn_Ok;
    JButton btn_Cancel;
    JPanel panel_params;
    ArrayList<ParametarView> parameters;

    JComboBox<KonfiguracijaDropdownView> combo_konfiguracije;
    JButton btn_DodajKonf;
    JButton btn_ObrisiKonf;
    //JButton btn_Osvezi;

    public PametniUredjajDialog(EditorView par, PametniUredjajView puv){
        this.puv = puv;
        parent = par;
        parameters = new ArrayList<ParametarView>();
        setSize(200, 200);
        setLocation(parent.getLocation());
        setTitle("Configure smart device");

        OsveziDijalog();

        setModal(true);
    }

    private void OsveziDijalog()
    {
        getContentPane().removeAll();
        repaint();

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(2,2,2,2);
        PametniUredjaj pu = puv.getPametniUredjaj();

        // KONFIGURACIJE
        combo_konfiguracije = new JComboBox<KonfiguracijaDropdownView>();
        combo_konfiguracije.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<Parametar> parametri = ((KonfiguracijaDropdownView) combo_konfiguracije.getSelectedItem()).konfiguracija.getParametri();
                for(int i = 0; i < parametri.size(); i++){
                	if(i < parameters.size())
                		parameters.get(i).kopiraj(parametri.get(i));
                }				
			}
        });

        // --> Ucitaj konfiguracije za uredjaj
        for(Konfiguracija k : pu.getMetaUredjaj().getKonfiguracije())
        {
            combo_konfiguracije.addItem(new KonfiguracijaDropdownView(k));
        }

        /*
        // Ako smo povukli barem jednu konfiguraciju, ucitaj vrednosti parametara iz nje
        if(combo_konfiguracije.getItemCount() > 0)
        {
            pu.setParametri(((KonfiguracijaDropdownView) combo_konfiguracije.getSelectedItem()).konfiguracija.getParametri());
        }
        */
        JPanel confpan = new JPanel(new GridLayout(2, 2, 20, 5));
        btn_DodajKonf = new JButton("Dodaj konf.");
        btn_DodajKonf.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                // Zatrazi ime
                String ime = JOptionPane.showInputDialog("Unesi naziv za novu konfiguraciju");
                if(ime != null){
                	ArrayList<Parametar> temp = new ArrayList<Parametar>();
                	for(ParametarView p : parameters){
                		if(p instanceof BinarniParamView){
                			BinarniParamView temp1 = (BinarniParamView)p;
                			BinarniParametar newP = new BinarniParametar();
                			newP.setNaziv(temp1.binParam.getNaziv());
                			newP.setPodatak(new Boolean(temp1.check.isSelected()));
                			temp.add(newP);
                		}
                		else if(p instanceof CelobrParamView){
                			CelobrParamView temp1 = (CelobrParamView)p;
                			CelobrojniParametar newP = new CelobrojniParametar();
                			newP.setNaziv(temp1.celobrParam.getNaziv());
                			newP.setPodatak(new Integer(Integer.parseInt(temp1.txt_value.getText())));
                			temp.add(newP);
                		}
                		else if(p instanceof RealniParamView){
                			RealniParamView temp1 = (RealniParamView)p;
                			RealniParametar newP = new RealniParametar();
                			newP.setNaziv(temp1.realParam.getNaziv());
                			newP.setPodatak(new Double(Double.parseDouble(temp1.txt_value.getText())));
                			temp.add(newP);
                		}
                		else{
                			TekstualniParamView temp1 = (TekstualniParamView)p;
                			TekstualniParametar newP = new TekstualniParametar();
                			newP.setNaziv(temp1.tekstualniParam.getNaziv());
                			newP.setPodatak(temp1.txt_value.getText());
                			temp.add(newP);
                		}

                	}
                    Konfiguracija konf = new Konfiguracija(ime, temp);
                    puv.getPametniUredjaj().getMetaUredjaj().addKonfiguracija(konf);
                    // Dodaj u dropdown
                    KonfiguracijaDropdownView kdv = new KonfiguracijaDropdownView(konf);
                    combo_konfiguracije.addItem(kdv);
                    // Odaberi
                    combo_konfiguracije.setSelectedItem(kdv);
                }
            }
        });
        confpan.add(btn_DodajKonf);
        confpan.add(combo_konfiguracije);
        btn_ObrisiKonf = new JButton("Obrisi trenutnu konf.");
        btn_ObrisiKonf.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                // Obrisi trenutno selektovanu konfiguraciju iz meta uredjaja
                Konfiguracija konf = ((KonfiguracijaDropdownView) combo_konfiguracije.getSelectedItem()).konfiguracija;
                puv.getPametniUredjaj().getMetaUredjaj().removeKonfiguracija(konf);

                // Obrisi trenutno selektovanu konfiguraciju iz dropdowna
                combo_konfiguracije.removeItemAt(combo_konfiguracije.getSelectedIndex());

                /*
                if(combo_konfiguracije.getSelectedItem() != null){
                    pu.setParametri(((KonfiguracijaDropdownView) combo_konfiguracije.getSelectedItem()).konfiguracija.getParametri());
                    OsveziDijalog();
                }
                */
            }
        });

        confpan.add(btn_ObrisiKonf);
        /*
        btn_Osvezi = new JButton("Osvezi");
        btn_Osvezi.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                // Refresh
                pu.setParametri(((KonfiguracijaDropdownView) combo_konfiguracije.getSelectedItem()).konfiguracija.getParametri());
                OsveziDijalog();
            }
        });
        confpan.add(btn_Osvezi);
        */
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        add(confpan, gbc);
        

        // Izgradi view za svaki parametar i dodaj na panel
        panel_params = new JPanel(new GridLayout(puv.getPametniUredjaj().getParametri().size(), 1, 1, 5));

        //panel_params.setBackground(Color.GRAY);
        ParametarView pv = null;
        Parametar p;

        for(int i=0; i < puv.getPametniUredjaj().getParametri().size(); i++) {
            p = puv.getPametniUredjaj().getParametri().get(i);

            // Instanciraj odgovarajuci param view
            if(p instanceof RealniParametar) {
                pv = new RealniParamView((RealniParametar) p);
            }
            if(p instanceof CelobrojniParametar) {
                pv = new CelobrParamView((CelobrojniParametar) p);
            }
            if(p instanceof BinarniParametar) {
                pv = new BinarniParamView((BinarniParametar) p);
            }
            if(p instanceof TekstualniParametar) {
            	pv = new TekstualniParamView((TekstualniParametar) p);
            }

            // Dodaj param view u panel na odg nacin preko layouta
            gbc.gridy = i;
            add(pv, gbc);
            
            parameters.add(pv);
            panel_params.add(pv);
        }
        gbc.gridy++;
        add(panel_params, gbc);

        // Dugmici za ok i cancel
        btn_Cancel = new JButton("Cancel");
        btn_Cancel.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                // Close dialog
                dispose();
            }
        });

        btn_Ok = new JButton("Ok");
        btn_Ok.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                // Push changes to model
                for(ParametarView pv : parameters) pv.PushToModel();

                /*
                // Push changes to conf
                Konfiguracija k = ((KonfiguracijaDropdownView) combo_konfiguracije.getSelectedItem()).konfiguracija;
                puv.getPametniUredjaj().getMetaUredjaj().updateKonfiguracija(k, puv.getPametniUredjaj().getParametri());
				*/

                System.out.println("Sacuvani podaci za pametni uredjaj " +
                        puv.getPametniUredjaj().getMetaUredjaj().getNaziv());
                // TODO ovo treba??
                dispose();
            }
        });

        JPanel panel = new JPanel(new GridLayout());
        panel.add(btn_Cancel);
        panel.add(btn_Ok);

        gbc.gridy++;
        add(panel, gbc);

        pack();
        revalidate();
        repaint();
    }


    public void run() {
        setVisible(true);
    }

}
