package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


public class CreateFileDialog extends JDialog implements ActionListener{
	JButton ok;
	JButton cancel;
	JButton openFileChooser;
	JTextField fileNameField;
	JTextField directoryName;
	JLabel fileNameLabel;
	JLabel spacer1;
	JLabel spacer2;
	
	private String path; //rezultat izvrsenja dijaloga, apsolutna putanja do novokreiranog fajla
	
	JLabel format;
	
	ButtonGroup group;
	JRadioButton regular;
	JRadioButton json;
	JRadioButton xml;
	
	public CreateFileDialog(EditorView parent){
		//construct gui
		setLocation(parent.getLocation().x + 700, parent.getLocation().y + 500);
		setSize(new Dimension(parent.getWidth() / 2, parent.getHeight() / 2));
		setTitle("Create new smart home");
		this.ok = new JButton("Ok");
		this.cancel = new JButton("Cancel");
		this.openFileChooser = new JButton("Choose directory");
		this.fileNameField = new JTextField();
		this.fileNameLabel = new JLabel("File name");
		this.directoryName = new JTextField();
		this.spacer1 = new JLabel(" ");
		this.spacer2 = new JLabel(" ");
		this.format = new JLabel("Select file type");
		
		this.group = new ButtonGroup();
		this.regular = new JRadioButton(".kuca");
		this.json = new JRadioButton(".json");
		this.xml = new JRadioButton(".xml");
		
		group.add(regular);
		group.add(json);
		group.add(xml);
		
		JPanel panel = new JPanel();
	    panel.setLayout(new GridBagLayout());
	    GridBagConstraints gbc = new GridBagConstraints();
	    gbc.insets = new Insets(2,2,2,2);
	    
	    gbc.fill = GridBagConstraints.HORIZONTAL;
	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    panel.add(fileNameLabel, gbc);
	    
	    gbc.gridwidth = 2;
	    gbc.gridx = 1;
	    gbc.gridy = 0;
	    panel.add(fileNameField, gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 0;
	    gbc.gridy = 1;
	    panel.add(openFileChooser, gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 1;
	    gbc.gridy = 1;
	    panel.add(directoryName, gbc);
	    
	    gbc.gridx = 0;
	    gbc.gridy = 2;
	    panel.add(spacer1,gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 0;
	    gbc.gridy = 3;
	    panel.add(format, gbc);
	    
	    gbc.gridx = 1;
	    gbc.gridy = 3;
	    panel.add(regular, gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 0;
	    gbc.gridy = 4;
	    panel.add(json, gbc);
	    
	    gbc.gridx = 1;
	    gbc.gridy = 4;
	    panel.add(xml, gbc);
	    
	    gbc.gridx = 0;
	    gbc.gridy = 5;
	    panel.add(spacer2,gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 0;
	    gbc.gridy = 6;
	    panel.add(ok, gbc);
	    
	    gbc.gridx = 1;
	    gbc.gridy = 6;
	    panel.add(cancel, gbc);
	    
	    getContentPane().add(panel);

	    //add listeners to buttons
	    ok.addActionListener(this);
	    cancel.addActionListener(this);
	    openFileChooser.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser dialog = new JFileChooser();
				dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);				
				int retval = dialog.showDialog(null, "Create");
				if(retval == JFileChooser.APPROVE_OPTION){
					directoryName.setText(dialog.getSelectedFile().getAbsolutePath());
				}
			}
		});
	    
	    subscribeOkButton();
	 
	    pack();
	    setModal(true);

	}
	
	void testToActivateButton(){
		this.ok.setEnabled(!fileNameField.getText().trim().isEmpty() && 
				!directoryName.getText().trim().isEmpty() && (regular.isSelected() || json.isSelected() || xml.isSelected()));
	}
	
	void subscribeOkButton(){
		this.ok.setEnabled(false);
	    this.fileNameField.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				testToActivateButton();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				testToActivateButton();				
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				testToActivateButton();				
			}
		});
	    
	    this.directoryName.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				testToActivateButton();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				testToActivateButton();				
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				testToActivateButton();				
			}
		});
	    
	    this.regular.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				testToActivateButton();
			}
		});
	    
	    this.json.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				testToActivateButton();
			}
		});	   
	    
	    this.xml.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				testToActivateButton();
			}
		});
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source == ok){
			path = directoryName.getText() + System.getProperty("file.separator") + fileNameField.getText();
			if(regular.isSelected()){
				path += regular.getText();
			}else if(json.isSelected()){
				path += json.getText();
			}else{
				path += xml.getText();
			}
		}else{
			path = null;
		}
		setVisible(false);
		dispose();
	}
	
	public String run(){
		this.setVisible(true);
		return path;
	}

	public String getPath() {
		return path;
	}	
}
