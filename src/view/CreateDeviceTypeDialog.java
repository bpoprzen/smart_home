package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import model.Biblioteka;
import model.Konfiguracija;
import model.MetaPametniUredjaj;
import model.parametar.Parametar;

public class CreateDeviceTypeDialog extends JDialog implements ActionListener{
	private EditorView parent;
	private JButton buttonOk;
	private JButton buttonCancel;
	private JTextArea deviceNameArea;
	private JButton addParameter;
	private JButton removeParameter;
	private JComboBox<ParametarDropdownView> parameterList;
	private MetaPametniUredjaj device;
	private boolean result;
	
	public CreateDeviceTypeDialog(EditorView parent) {
		this.parent = parent;
		setLocation(parent.getLocation().x + 700, parent.getLocation().y + 500);
		setSize(new Dimension(parent.getWidth() / 2, parent.getHeight() / 2));
		
		device = new MetaPametniUredjaj();
		
		JLabel deviceNameLabel = new JLabel("Device name");
		this.deviceNameArea = new JTextArea();
		this.buttonCancel = new JButton("Cancel");
		this.buttonOk = new JButton("Ok");
		this.buttonOk.setEnabled(false);
		this.addParameter = new JButton("Add parameter");
		this.removeParameter = new JButton("Remove current parameter");
		this.removeParameter.setEnabled(false);
		this.parameterList = new JComboBox<ParametarDropdownView>();
		
		JPanel panel = new JPanel();
	    panel.setLayout(new GridBagLayout());
	    GridBagConstraints gbc = new GridBagConstraints();
	    gbc.insets = new Insets(2,2,2,2);
	    
	    gbc.fill = GridBagConstraints.HORIZONTAL;
	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    panel.add(deviceNameLabel, gbc);
	    
	    gbc.gridwidth = 2;
	    gbc.gridx = 1;
	    gbc.gridy = 0;
	    panel.add(this.deviceNameArea, gbc);
	    
	    gbc.gridwidth = 2;
	    gbc.gridx = 0;
	    gbc.gridy = 1;
	    panel.add(new JLabel(" "), gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 0;
	    gbc.gridy = 2;
	    panel.add(new JLabel("Parameters: "), gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 1;
	    gbc.gridy = 2;
	    panel.add(this.parameterList, gbc);
	    
	    gbc.gridwidth = 2;
	    gbc.gridx = 0;
	    gbc.gridy = 3;
	    panel.add(new JLabel(" "), gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 0;
	    gbc.gridy = 4;
	    panel.add(this.addParameter, gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 1;
	    gbc.gridy = 4;
	    panel.add(this.removeParameter, gbc);
	    
	    gbc.gridwidth = 2;
	    gbc.gridx = 0;
	    gbc.gridy = 5;
	    panel.add(new JLabel(" "), gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 0;
	    gbc.gridy = 6;
	    panel.add(this.buttonOk, gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 1;
	    gbc.gridy = 6;
	    panel.add(this.buttonCancel, gbc);
	  
	    getContentPane().add(panel);
		
		addListeners();
		
		setModal(true);
		pack();
	}
	
	void testToActivateButton(){
		this.buttonOk.setEnabled(! this.deviceNameArea.getText().trim().isEmpty() && ! Biblioteka.getInstance().getMetaUredjaji().
																						containsKey(deviceNameArea.getText().trim()));
	}
	
	void addListeners(){
		this.buttonOk.addActionListener(this);
		this.buttonCancel.addActionListener(this);
		this.removeParameter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				parameterList.removeItemAt(parameterList.getSelectedIndex());
				if(parameterList.getSelectedItem() == null){
					removeParameter.setEnabled(false);
				}
			}
		});
		this.addParameter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				CreateParameterDialog pd = new CreateParameterDialog(parent);
				if(pd.run()){
					parameterList.insertItemAt(pd.getResult(), 0);
					parameterList.setSelectedIndex(0);
					removeParameter.setEnabled(true);
				}
			}
		});
		this.deviceNameArea.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				testToActivateButton();			
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				testToActivateButton();		
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				testToActivateButton();
			}
		});
		
	}
	
	public MetaPametniUredjaj getDevice() {
		return device;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source == buttonOk){
			device.setNaziv(this.deviceNameArea.getText());
			ArrayList<Parametar> params = new ArrayList<Parametar>();
			for(int i = 0; i < parameterList.getItemCount(); i++){
				params.add(parameterList.getItemAt(i).getParametar());
			}
			device.setParametri(params);
			device.setKonfiguracije(new ArrayList<Konfiguracija>());
			result = true;
		}else{
			result = false;
		}
		setVisible(false);
		dispose();
		
	}
	
	public boolean run(){
		this.setVisible(true);
		return result;
	}
}
