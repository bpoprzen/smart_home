package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import model.parametar.BinarniParametar;
import model.parametar.CelobrojniParametar;
import model.parametar.RealniParametar;
import model.parametar.TekstualniParametar;

public class CreateParameterDialog extends JDialog implements ActionListener{
	private EditorView parent;
	private ParametarDropdownView result;
	private boolean status;
	
	private JButton buttonOk;
	private JButton buttonCancel;
	private ButtonGroup group;
	private JRadioButton binary;
	private JRadioButton integral;
	private JRadioButton real;
	private JRadioButton textual;
	private JTextArea paramName;
	
	
	public CreateParameterDialog(EditorView parent){
		this.parent = parent;
		setLocation(parent.getLocation().x + 700, parent.getLocation().y + 500);
		setSize(new Dimension(parent.getWidth() / 2, parent.getHeight() / 2));
		this.paramName = new JTextArea();
		this.buttonCancel = new JButton("Cancel");
		this.buttonOk = new JButton("Ok");
		this.buttonOk.setEnabled(false);
		this.group = new ButtonGroup();
		this.binary = new JRadioButton("Binary");
		this.integral = new JRadioButton("Integral");
		this.real = new JRadioButton("Real");
		this.textual = new JRadioButton("Textual");

		group.add(binary);
		group.add(integral);
		group.add(real);
		group.add(textual);
		
		JPanel panel = new JPanel();
	    panel.setLayout(new GridBagLayout());
	    GridBagConstraints gbc = new GridBagConstraints();
	    gbc.insets = new Insets(2,2,2,2);
	    
	    gbc.fill = GridBagConstraints.HORIZONTAL;
	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    panel.add(new JLabel("Parameter name: "), gbc);
	    
	    gbc.gridwidth = 2;
	    gbc.gridx = 1;
	    gbc.gridy = 0;
	    panel.add(paramName, gbc);
	    
	    gbc.gridwidth = 2;
	    gbc.gridx = 0;
	    gbc.gridy = 1;
	    panel.add(new JLabel(" "), gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 0;
	    gbc.gridy = 2;
	    panel.add(new JLabel("Parameter type"), gbc);
	    
	    gbc.gridx = 1;
	    gbc.gridy = 2;
	    panel.add(binary, gbc);
	    
	    gbc.gridx = 1;
	    gbc.gridy = 3;
	    panel.add(textual, gbc);
		
	    gbc.gridx = 1;
	    gbc.gridy = 4;
	    panel.add(real, gbc);
	    
	    gbc.gridx = 1;
	    gbc.gridy = 5;
	    panel.add(integral, gbc);
		
	    gbc.gridwidth = 2;
	    gbc.gridx = 0;
	    gbc.gridy = 6;
	    panel.add(new JLabel(" "), gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 0;
	    gbc.gridy = 7;
	    panel.add(buttonOk, gbc);
	    
	    gbc.gridwidth = 1;
	    gbc.gridx = 1;
	    gbc.gridy = 7;
	    panel.add(buttonCancel, gbc);
	    
	    getContentPane().add(panel);
	    addListeners();
	    setModal(true);
	    pack();
	}
	
	void testToActivateButton(){
		this.buttonOk.setEnabled(!this.paramName.getText().trim().isEmpty() && (binary.isSelected() || textual.isSelected() || integral.isSelected() || real.isSelected()));
	}
	
	void addListeners(){
		this.buttonOk.addActionListener(this);
		this.buttonCancel.addActionListener(this);
		this.paramName.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				testToActivateButton();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				testToActivateButton();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				testToActivateButton();				
			}
		});
		this.binary.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				testToActivateButton();
				
			}
		});
		this.textual.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						testToActivateButton();
						
					}
				});
		this.integral.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				testToActivateButton();
				
			}
		});
		this.real.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				testToActivateButton();
				
			}
		});
	}
	
	public boolean run(){
		setVisible(true);
		return status;
	}
	
	public ParametarDropdownView getResult() {
		return result;
	}

	public void setResult(ParametarDropdownView result) {
		this.result = result;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(this.buttonOk)){
			status = true;
			if(binary.isSelected()){
				result = new ParametarDropdownView(new BinarniParametar(this.paramName.getText(), null));
			}else if(textual.isSelected()){
				result = new ParametarDropdownView(new TekstualniParametar(this.paramName.getText(), null));
			}else if(integral.isSelected()){
				result = new ParametarDropdownView(new CelobrojniParametar(this.paramName.getText(), null));
			}else{
				result = new ParametarDropdownView(new RealniParametar(this.paramName.getText(), null));
			}
		}else{
			status = false;
		}
		setVisible(false);
		dispose();
	}
}
