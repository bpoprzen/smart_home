package view;

import model.Konfiguracija;

public class KonfiguracijaDropdownView {
    Konfiguracija konfiguracija;

    KonfiguracijaDropdownView(Konfiguracija konf){
        konfiguracija = konf;
    }

    public Konfiguracija getKonfiguracija() {
        return konfiguracija;
    }

    public void setKonfiguracija(Konfiguracija konf) {
        konfiguracija = konf;
    }

    @Override
    public String toString() {
        return konfiguracija.getNaziv();
    }
}
