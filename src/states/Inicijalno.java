package states;

import model.Aplikacija;

public class Inicijalno extends StanjeAplikacije{

	public Inicijalno(Aplikacija kontekst) {
		super(kontekst);
	}

	@Override
	public void kreiranjeZatrazeno() {
		if(this.kontekst.kreiraj())
			this.kontekst.setStanje(new Editovanje(this.kontekst));		
	}
	
	@Override
	public void ucitavanjeZatrazeno() {
		if(this.kontekst.ucitaj())
			this.kontekst.setStanje(new Editovanje(this.kontekst));
	}

	@Override
	public void zatvaranjeZatrazeno() {
		// nema funkcionalnost
	}

	@Override
	public void cuvanjeZatrazeno() {
		// nema funkcionalnost		
	}

	@Override
	public void exportZatrazen() {
		// nema funkcionalnost				
	}

	@Override
	public void entry() {
		this.kontekst.setUcitanObjekat(null);
	}

	
	
}
