package states;

import model.Aplikacija;

public class Editovanje extends StanjeAplikacije{

	public Editovanje(Aplikacija kontekst) {
		super(kontekst);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void kreiranjeZatrazeno() {
		this.kontekst.sacuvaj();
		this.kontekst.zatvori();
		if(this.kontekst.kreiraj())
			this.kontekst.setStanje(this);
		else
			this.kontekst.setStanje(new Inicijalno(this.kontekst));
	}
	
	@Override
	public void ucitavanjeZatrazeno() {
		this.kontekst.sacuvaj();
		this.kontekst.zatvori();
		if(this.kontekst.ucitaj())
			this.kontekst.setStanje(this);
		else
			this.kontekst.setStanje(new Inicijalno(this.kontekst));
	}

	@Override
	public void zatvaranjeZatrazeno() {
		this.kontekst.sacuvaj();
		this.kontekst.zatvori();
		this.kontekst.setStanje(new Inicijalno(this.kontekst));
	}


	@Override
	public void cuvanjeZatrazeno() {
		this.kontekst.sacuvaj();
		this.kontekst.setStanje(this);
	}


	@Override
	public void exportZatrazen() {
		this.kontekst.export();
		this.kontekst.setStanje(this);
	}


	@Override
	public void entry() {
		// TODO Auto-generated method stub
		
	}

};
