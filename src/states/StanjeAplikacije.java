package states;

import model.Aplikacija;

public abstract class StanjeAplikacije {
	protected Aplikacija kontekst;
	
	public abstract void kreiranjeZatrazeno();
	public abstract void ucitavanjeZatrazeno();
	public abstract void zatvaranjeZatrazeno();
	public abstract void cuvanjeZatrazeno();
	public abstract void exportZatrazen();
	public abstract void entry();
	
	public StanjeAplikacije(Aplikacija kontekst){
		this.kontekst = kontekst;
	}

}
