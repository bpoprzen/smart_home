package events;

public interface LoadObjectEventListener {
	void eventPerformed(LoadObjectEvent event);
}
