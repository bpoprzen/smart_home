package events;

import java.util.ArrayList;
import java.util.EventObject;

public class CloseObjectEvent extends EventObject{
	public static ArrayList<CloseObjectEventListener> listeners;

	public CloseObjectEvent(Object source) {
		super(source);
		// TODO Auto-generated constructor stub
	}

	static{
		listeners = new ArrayList<CloseObjectEventListener>();
	}
	
	public void notifyListeners(){
		for(CloseObjectEventListener eventListener : listeners){
			eventListener.eventPerformed(this);
		}
	}
	
	public static void addListeners(CloseObjectEventListener event){
		listeners.add(event);
	}
}
