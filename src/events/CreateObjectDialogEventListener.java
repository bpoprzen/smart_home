package events;

public interface CreateObjectDialogEventListener {
	public void eventPerformed(CreateObjectDialogEvent e);
}
