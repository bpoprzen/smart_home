package events;

public interface CloseObjectEventListener {
	public void eventPerformed(CloseObjectEvent event);
}
