package events;

import java.util.ArrayList;
import java.util.EventObject;

import model.PametniObjekat;

public class LoadObjectEvent extends EventObject{
	/*
	 * Dogadjaj vezan za ucitavanje kuce - ili open house ili new house
	 * Obavestava listener-e(StatusBar, ToolBar i glavni panel) da se prikazu
	 */
	private PametniObjekat ucitanObjekat;
	public static ArrayList<LoadObjectEventListener> listeners;
	
	static{
		//najbolje je init-ovati staticke atribute u statickom bloku
		listeners = new ArrayList<LoadObjectEventListener>();
	}
	
	public LoadObjectEvent(Object arg0, PametniObjekat ucitanObjekat) {
		super(arg0);
		this.ucitanObjekat = ucitanObjekat;
	}

	public PametniObjekat getUcitanObjekat() {
		return ucitanObjekat;
	}

	public void setUcitanObjekat(PametniObjekat ucitanObjekat) {
		this.ucitanObjekat = ucitanObjekat;
	}
	
	public void notifyListeners(){
		for(LoadObjectEventListener eventListener : listeners){
			eventListener.eventPerformed(this);
		}
	}
	
	public static void addListeners(LoadObjectEventListener event){
		listeners.add(event);
	}
	
	
}
