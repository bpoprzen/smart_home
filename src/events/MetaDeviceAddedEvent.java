package events;

import java.util.ArrayList;
import java.util.EventObject;

import model.MetaPametniUredjaj;

public class MetaDeviceAddedEvent extends EventObject{
	private MetaPametniUredjaj device;
	private static ArrayList<MetaDeviceAddedEventListeners> listeners;
	
	static{
		listeners = new ArrayList<MetaDeviceAddedEventListeners>();
	}
	
	public MetaDeviceAddedEvent(Object arg0, MetaPametniUredjaj device) {
		super(arg0);
		this.device = device;
	}
	
	public void notifyListeners(){
		for(MetaDeviceAddedEventListeners eventListener : listeners){
			eventListener.eventPerformed(this);
		}
	}
	
	public static void addListeners(MetaDeviceAddedEventListeners event){
		listeners.add(event);
	}

	public MetaPametniUredjaj getDevice() {
		return device;
	}

	
}
