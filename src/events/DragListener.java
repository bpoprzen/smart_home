package events;

import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.MouseEvent;
import utility.IsKeyDown;

public class DragListener extends MouseInputAdapter{
    Point location;
    MouseEvent pressed;

    public void mousePressed(MouseEvent me)
    {
        pressed = me;
    }

    public void mouseDragged(MouseEvent me)
    {
        Component component = me.getComponent();

        // Moze se vuci samo ako se drzi ALT
        if(!IsKeyDown.ALT) {
            component.setEnabled(true);
            return;
        }
        component.setEnabled(false);
        location = component.getLocation(location);
        int x = location.x - pressed.getX() + me.getX();
        int y = location.y - pressed.getY() + me.getY();
        component.setLocation(x, y);
    }

    public void mouseReleased(MouseEvent me)
    {
        me.getComponent().setEnabled(true);
    }
}
