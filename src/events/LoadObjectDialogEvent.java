package events;

import java.util.ArrayList;
import java.util.EventObject;

public class LoadObjectDialogEvent extends EventObject{
	/*
	 * Dogadjaj se prosledjuje gui komponenti. GUI komponenta reaguje izvrsavanjem JFileChooser diajaloga
	 */

	private String apsolutePath; //odabrana putanja kao rezultat izvrsenja dijaloga
	static ArrayList<LoadObjectDialogEventListener> listeners;
	
	static{
		listeners = new ArrayList<LoadObjectDialogEventListener>();
	}
	
	public LoadObjectDialogEvent(Object source) {
		super(source);
	}

	public static void addListeners(LoadObjectDialogEventListener l){
		listeners.add(l);
	}
	
	public void notifyListeners(){
		for(LoadObjectDialogEventListener l : listeners){
			l.eventPerformed(this);
		}
	}

	public String getApsolutePath() {
		return apsolutePath;
	}

	public void setApsolutePath(String apsolutePath) {
		this.apsolutePath = apsolutePath;
	}
	
}
