package events;

import java.util.ArrayList;
import java.util.EventObject;

public class SaveObjectDialogEvent extends EventObject{
	private boolean saveRequested;
	private String nazivObjekta;
	
	static ArrayList<SaveObjectDialogEventListener> listeners;
	public SaveObjectDialogEvent(Object source, String nazivObjekta) {
		super(source);
		this.saveRequested = false;
		this.nazivObjekta = nazivObjekta;
	}
	
	static{
		listeners = new ArrayList<SaveObjectDialogEventListener>();
	}
	
	public static void addListeners(SaveObjectDialogEventListener l){
		listeners.add(l);
	}
	
	public void notifyListeners(){
		for(SaveObjectDialogEventListener l : listeners){
			l.eventPerformed(this);
		}
	}

	public boolean isSaveRequested() {
		return saveRequested;
	}

	public void setSaveRequested(boolean saveRequested) {
		this.saveRequested = saveRequested;
	}

	public String getNazivObjekta() {
		return nazivObjekta;
	}

	public void setNazivObjekta(String nazivObjekta) {
		this.nazivObjekta = nazivObjekta;
	}
	
	
	
	

}
