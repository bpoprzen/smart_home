package events;

public interface MetaDeviceAddedEventListeners {
	public void eventPerformed(MetaDeviceAddedEvent e);
}
