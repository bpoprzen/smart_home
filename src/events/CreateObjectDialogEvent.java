package events;

import java.util.ArrayList;
import java.util.EventObject;

public class CreateObjectDialogEvent extends EventObject{
	/*
	 * Dogadjaj se prosledjuje gui komponenti. GUI komponenta reaguje izvrsavanjem CreateFileDialog diajaloga
	 */

	private String apsolutePath; //odabrana putanja kao rezultat izvrsenja dijaloga
	static ArrayList<CreateObjectDialogEventListener> listeners;
	
	static{
		listeners = new ArrayList<CreateObjectDialogEventListener>();
	}
	
	public CreateObjectDialogEvent(Object source) {
		super(source);
	}

	public static void addListeners(CreateObjectDialogEventListener l){
		listeners.add(l);
	}
	
	public void notifyListeners(){
		for(CreateObjectDialogEventListener l : listeners){
			l.eventPerformed(this);
		}
	}

	public String getApsolutePath() {
		return apsolutePath;
	}

	public void setApsolutePath(String apsolutePath) {
		this.apsolutePath = apsolutePath;
	}
	
}
