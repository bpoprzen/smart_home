package events;

public interface SaveObjectDialogEventListener {
	public void eventPerformed(SaveObjectDialogEvent e);
}
