package events;

public interface LoadObjectDialogEventListener {
	public void eventPerformed(LoadObjectDialogEvent e);
}
