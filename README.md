# Smart Home projekat
> Grafički editor za projektovanje kuće kojeg koristi projektant i koji se koristi za snimanje konfiguracije kuće. 
Konfiguracija kuće se potom može učitati u aplikaciju koje koriste ukućani za upravljanje kućom.



Predmet: Specifikacija i modeliranje softvera

Tim: 6

Članovi:

| Ime i prezime        | BRIND          | github  |
| -------------------- |--------------- | ------- |
| Milan Đurić          |   SW 12/2017    | https://gitlab.com/DjukaBazuka16 |
| Stefan Stegić        |   SW 61/2017    | https://gitlab.com/phuskus       |
| Aleksandar Vujinović | |  |
| Bojan Popržen        |   SW 16/2017    | https://github.com/ele7ija |
